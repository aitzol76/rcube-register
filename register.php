<!--
/*
 * Copyright 2018 Wproject - Aitzol Berasategi - https://aitzol.eus
 *
 * This file is part of Notes.
 *
 * This software is free; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
-->



<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Roundcube Webmail</title>
    <link rel="stylesheet" href="skins/larry/styles.min.css" />
    <link rel="stylesheet" href="plugins/jqueryui/themes/larry/jquery-ui.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/locales.js"></script>
  </head>
  <body>
  <?php
    include_once 'db.php';
    $error = false;
    $errorCode = array();
    function errorControl() {
      echo "<script>setTimeout(function(){var messages=document.getElementById('messagestack');messages.setAttribute('class','message');}, 3000);</script>";
    } 
    if ( isset($_POST['submit']) ) {

      // clean user inputs to prevent sql injections
      $name = trim($_POST['name']);
      $name = strip_tags($name);
      $name = htmlspecialchars($name);

      $surname = trim($_POST['surname']);
      $surname = strip_tags($surname);
      $surname = htmlspecialchars($surname);

      $email = trim($_POST['email']);
      $email = strip_tags($email);
      $email = htmlspecialchars($email);
      
      $password = trim($_POST['password']);
      $password = strip_tags($password);
      $password = htmlspecialchars($password);

      $password2 = trim($_POST['password2']);
      $password2 = strip_tags($password2);
      $password2 = htmlspecialchars($password2);

      // name validation
      if (empty($name)) {
       $errorCode[] = "0";
      } else if (strlen($name) < 3) {
       $errorCode[] = "1";
      } else if (!preg_match("/^[a-zA-Z ]+$/",$name)) {
       $errorCode[] = "2";
      }

      // surname validation
      if (empty($surname)) {
       $errorCode[] = "3";
      } else if (strlen($surname) < 3) {
       $errorCode[] = "4";
      } else if (!preg_match("/^[a-zA-Z ]+$/",$surname)) {
       $errorCode[] = "5";
      }

      // email validation
      if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) {
       $errorCode[] = "6";
      } else {
       // check email exist or not
       $query = "SELECT email FROM virtual_users WHERE email='$email'";
       $result = mysqli_query($con,$query);
       $count = mysqli_num_rows($result);
       if($count!=0){
        $errorCode[] = "7";
       }
      }

      // password validation
      if (empty($password)){
       $errorCode[] = "8";
      } else if(strlen($password) < 8) {
       $errorCode[] = "9";
      }

      if (empty($password2)){
        $errorCode[] = "10";
      } else if($password2 != $password){
        $errorCode[] = "11";
      }
      
      // registration date
      $reg_date = date("Y-m-d H:i:s");

      $query = "INSERT into `virtual_users` (domain_id, email, password) 
      VALUES ('1','$email', ENCRYPT('$password', CONCAT('$5$', SUBSTRING(SHA(RAND()), -16))) )";
      if(!$error){
        $result = mysqli_query($con,$query);
        if($result){
          $user_id = mysqli_insert_id($con);
          $query2 = "INSERT into `virtual_users_data` (user_id, name, surname,date) VALUES ('$user_id', '$name','$surname','$reg_date')";
          $result2 = mysqli_query($con,$query2);
          if($result2){
            header("Location: index.php");
            die();
          }else{
            $errorCode[] = "12";
            errorControl();            
          }
        }else{
          $errorCode[] = "12";
          errorControl();
        }
      }else{
        errorControl();
      }

    }
  ?>

  <div id="login-form">
    <div class="box-inner">
      <img src="skins/larry/images/roundcube_logo.png" onClick="location.href='index.php'" id="logo" alt="Roundcube Webmail">
      <div class="form">
        <form name="registration" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
          <table>
            <tbody>
              <tr>
                <td class="title">
                  <label for="rcmname" id="fn">Name</label>
                </td>
                <td class="input"><input name="name" id="rcmname" required="required" size="40" autocapitalize="off" autocomplete="off" type="text">
                </td>
              </tr>
              <tr>
                <td class="title">
                  <label for="rcmsurname" id="sn">Surname</label>
                </td>
                <td class="input"><input name="surname" id="rcmsurname" required="required" size="40" autocapitalize="off" autocomplete="off" type="text">
                </td>
              </tr>              
              <tr>
                <td class="title">
                  <label for="rcmloginuser" id="un">Username</label>
                </td>
                <td class="input"><input name="email" id="rcmloginuser" required="required" size="40" autocapitalize="off" autocomplete="off" type="email">
                </td>
              </tr>
              <tr> 
                <td class="title">
                  <label for="rcmloginpwd" id="pw">Password</label>
                </td>
                <td class="input"><input name="password" id="rcmloginpwd" required="required" size="40" autocapitalize="off" autocomplete="off" type="password">
                </td>
              <tr>
                <td class="title">
                  <label for="rcmloginpwd" id="pw2">Comfirm password</label>
                </td>
                <td class="input"><input name="password2" id="rcmloginpwd2" required="required" size="40" autocapitalize="off" autocomplete="off" type="password">
                </td>
              </tr>
            </tbody>
          </table>
          <p class="formbuttons"><input name="submit" id="reg" class="button mainaction" value="Register" type="submit"></p>
        </form>
      </div>
    </div>
  </div>
  <div id="bottomline" role="contentinfo">Roundcube Webmail</div>
  <div id="messagestack">
  <?php 

    if (!empty($errorCode)) {
      $length = sizeof($errorCode);
      for ($i = 0; $i < $length; $i++) {
        echo "<div class='error' role='alert' id=".$errorCode[$i]."></div>";
      }
    }

  ?>

  </div> 
  <script src="js/translate.js"></script>
</body>
</html>
