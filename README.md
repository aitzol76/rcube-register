# Roundcube Register

Simple registration page for Roundcube

## Getting Started

These instructions will get you a copy of the project up and running on your server. After the install you will have a Registration page in your Roundcube instance.

### Prerequisites

Mailserver running on Debian based LAMP server.
Roundcube Webmail installed.

### Installing

Clone the proyect in the proper path.

```
cd /var/www
git clone https://gitlab.com/aitzol76/rcube-register.git
```

Create the right links.

```
ln -s /var/www/html/webmail/db.php /var/lib/roundcube/db.php
ln -s /var/www/html/webmail/register.php /var/lib/roundcube/register.php
ln -s /var/www/html/webmail/css /var/lib/roundcube/css
ln -s /var/www/html/webmail/js /var/lib/roundcube/js
```

## Test

Point to..

```
http://localhost/roundcube/register.php
```

If you wish, you can put a button on the Roundcube login to access the register page. Simply add this line to <strong>roundcube/skins/larry/templates/login.html</strong> file

```
<p class="formbuttons"><input name="register" id="reg" class="button mainaction" onClick="location.href='register.php'" value="Register" type="button"></p>
```

