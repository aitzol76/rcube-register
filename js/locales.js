/*
 * Copyright 2018 Wproject - Aitzol Berasategi - https://aitzol.eus
 *
 * This file is part of Notes.
 *
 * This software if free; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var tr = {
	en: {
		'Please enter your full name.':'Please enter your full name.',
		'Name must have at least 3 characters.':'Name must have at least 3 characters.',
		'Name must contain alphabets and space.':'Name must contain alphabets and space.',
		'Please enter your full surname.':'Please enter your full surname.',
		'Surname must have at least 3 characters.':'Surname must have at least 3 characters.',
		'Surname must contain alphabets and space.':'Surname must contain alphabets and space.',
		'Please enter valid email address.':'Please enter valid email address.',
		'Provided Email is already in use.':'Provided Email is already in use.',
		'Please enter password.':'Please enter password.',
		'Password must have at least 8 characters.':'Password must have at least 8 characters.',
		'Please retype password.':'Please retype password.',
		'Passwords do not match.':'Passwords do not match.',
		'Database error. Please try again later.':'Database error. Please try again later.',
		'Name':'Name',
		'Surname':'Surname',
		'Username':'Username',
		'Password':'Password',
		'Repeat password':'Repeat password',
		'Register':'Register'
	},
	eu: {
		'Please enter your full name.':'Mesedez sartu zure izena.',
		'Name must have at least 3 characters.':'Izenak gutxienez hiru hizki izan behar ditu.',
		'Name must contain alphabets and space.':'Izenak alfabetuko hizkiak eta hutsuneak izan ditzake.',
		'Please enter your full surname.':'Mesedez sartu zure abizena.',
		'Surname must have at least 3 characters.':'Abizenak gutxienez hiru hizki izan behar ditu.',
		'Surname must contain alphabets and space.':'Abizenak alfabetuko hizkiak eta hutsuneak izan ditzake.',
		'Please enter valid email address.':'Mesedez sartu email zuzena.',
		'Provided Email is already in use.':'Sartutako emaila dagoeneko erabiltzen ari da.',
		'Please enter password.':'Mesedez sartu pasahitza.',
		'Password must have at least 8 characters.':'Pasahitzak gutxienez 8 hizki izan behar ditu.',
		'Please retype password.':'Mesedez berretsi pasahitza.',
		'Passwords do not match.':'Pasahitzak ez datoz bat.',
		'Database error. Please try again later.':'Akatsa datubasean. Mesedez saiatu beranduago.',
		'Name':'Izena',
		'Surname':'Abizena',
		'Username':'Erabiltzaile-izena',
		'Password':'Pasahitza',
		'Repeat password':'Berretsi pasahitza',
		'Register':'Kontua sortu'
	},
	es: {
		'Please enter your full name.':'Por favor ingresa tu nombre.',
		'Name must have at least 3 characters.':'El nombre debe tener al menos 3 caracteres.',
		'Name must contain alphabets and space.':'El nombre debe contener letras y espacios.',
		'Please enter your full surname.':'Por favor ingresa tu apellido.',
		'Surname must have at least 3 characters.':'El apellido debe contener al menos 3 caracteres.',
		'Surname must contain alphabets and space.':'El apellido debe contener letras y espacios.',
		'Please enter valid email address.':'Por favor ingresa un email válido.',
		'Provided Email is already in use.':'El email ingresado ya se encuentra en uso.',
		'Please enter password.':'Por favor ingresa la contraseña.',
		'Password must have at least 8 characters.':'La contraseña debe tener al menos 8 caracteres.',
		'Please retype password.':'Por favor confirma la contraseña.',
		'Passwords do not match.':'Las contraseñas no coinciden.',
		'Database error. Please try again later.':'Error en la base de datos.Intentelo mas tarde.',
		'Name':'Nombre',
		'Surname':'Apellido',
		'Username':'Nombre de usuario',
		'Password':'Contraseña',
		'Repeat password':'Comfirmar contraseña',
		'Register':'Registrarse'
	}
};

