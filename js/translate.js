/*
 * Copyright 2018 Wproject - Aitzol Berasategi - https://aitzol.eus
 *
 * This file is part of Notes.
 *
 * This software is free; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



var navLang = navigator.language;
if (navLang == null){
	var lang = 'en';
}else if (navLang.indexOf('-') !== -1){
    lang = navLang.split('-')[0]; 
}else{
	var lang = navLang;
}

var maxErrNum = 13;
var errorDiv = document.getElementById('messagestack');
if (errorDiv.childElementCount > 0) { 
	for(i=0;i<maxErrNum;i++){
		var errorChild = document.getElementById(i);
		if(errorChild != null){
			switch (i){
				case 0: errorChild.innerHTML = tr[lang]['Please enter your full name.'];
				break;
				case 1: errorChild.innerHTML = tr[lang]['Name must have at least 3 characters.'];
				break;
				case 2: errorChild.innerHTML = tr[lang]['Name must contain alphabets and space.'];
				break;
				case 3: errorChild.innerHTML = tr[lang]['Please enter your full surname.'];
				break;
				case 4: errorChild.innerHTML = tr[lang]['Surname must have at least 3 characters.'];
				break;
				case 5: errorChild.innerHTML = tr[lang]['Surname must contain alphabets and space.'];
				break;
				case 6: errorChild.innerHTML = tr[lang]['Please enter valid email address.'];
				break;
				case 7: errorChild.innerHTML = tr[lang]['Provided Email is already in use.'];
				break;
				case 8: errorChild.innerHTML = tr[lang]['Please enter password.'];
				break;
				case 9: errorChild.innerHTML = tr[lang]['Password must have at least 8 characters.'];
				break;
				case 10: errorChild.innerHTML = tr[lang]['Please retype password.'];
				break;								
				case 11: errorChild.innerHTML = tr[lang]['Passwords do not match.'];
				break;	
				case 12: errorChild.innerHTML = tr[lang]['Database error. Please try again later.'];
				break;
			}
		}
	}

}

var fn = document.getElementById('fn');
fn.innerHTML = tr[lang]['Name'];
var sn = document.getElementById('sn');
sn.innerHTML = tr[lang]['Surname'];
var un = document.getElementById('un');
un.innerHTML = tr[lang]['Username'];
var pw = document.getElementById('pw');
pw.innerHTML = tr[lang]['Password'];
var pw2 = document.getElementById('pw2');
pw2.innerHTML = tr[lang]['Repeat password'];
var reg = document.getElementById('reg');
reg.value = tr[lang]['Register'];
